Name:           needrestart
Version:        3.1
Release:        2%{?dist}
Summary:        Restart daemons after library updates

License:        GPLv2+
URL:            https://github.com/liske/%{name}
Source0:        https://github.com/liske/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz
Source1:        debconf__needrestart.templates
Source2:        restart.d__auditd.service
Source3:        yum__plugin.py
Source4:        dnf__plugin.py

BuildArch:         noarch
BuildRequires:     perl
BuildRequires:     gettext
BuildRequires:     perl(ExtUtils::MakeMaker)
BuildRequires:     debconf
BuildRequires:     po-debconf
%if 0%{?fedora}
BuildRequires:     python3-devel
Requires:          python3-dnf
%else
Requires:          yum
%endif
Requires:          perl(Module::Find)
Requires:          perl(Module::ScanDeps)
Requires:          perl(Locale::TextDomain)
Requires:          perl(Proc::ProcessTable)
Requires:          perl(Sort::Naturally)
Requires:          perl(Term::ReadKey)
Requires:          perl(Debconf::Client::ConfModule)
Requires:          xz
Requires:          iucode-tool

%{?perl_default_filter}


%description
needrestart checks which daemons need to be restarted after library
upgrades. It is inspired by checkrestart from the debian-goodies
package.


%prep
%autosetup -n %{name}-%{version} -p 1

%build
%make_build


%install
%make_install
mkdir -p %{buildroot}/%{_mandir}/man1
cp man/needrestart.1 %{buildroot}/%{_mandir}/man1/
%find_lang %{name}
%find_lang needrestart-notify
# useless files
rm -rf %{buildroot}/%{perl_archlib}
# workaround for https://bugzilla.redhat.com/show_bug.cgi?id=1489569
cp %{SOURCE1} %{buildroot}/%{_datadir}/%{name}/needrestart.templates
# workaround for https://github.com/liske/needrestart/issues/75
cp %{SOURCE2} %{buildroot}/%{_sysconfdir}/%{name}/restart.d/auditd.service
chmod a+x %{buildroot}/%{_sysconfdir}/%{name}/restart.d/auditd.service
%if 0%{?fedora}
mkdir -p %{buildroot}/%{_sysconfdir}/dnf/plugins %{buildroot}/%{python3_sitelib}/dnf-plugins
echo -e "[main]\nenabled=1\n" >%{buildroot}/%{_sysconfdir}/dnf/plugins/needrestart.conf
cp %{SOURCE4} %{buildroot}/%{python3_sitelib}/dnf-plugins/needrestart.py
%else
mkdir -p %{buildroot}/%{_sysconfdir}/yum/pluginconf.d %{buildroot}/usr/lib/yum-plugins
echo -e "[main]\nenabled=1\n" >%{buildroot}/%{_sysconfdir}/yum/pluginconf.d/needrestart.conf
cp %{SOURCE3} %{buildroot}/usr/lib/yum-plugins/needrestart.py
%endif


# About executable files in the /etc directory:
#   The 'README.needrestart' files in /etc/needrestart/restart.d/ and
#   /etc/needrestart/notify.d/ explicitly say the files will only be
#   considered if they are executables. There's nothing said for
#   /etc/needrestart/hook.d/ but I guess this is the same logic.
%files -f %{name}.lang -f needrestart-notify.lang
%license COPYING
%doc README.md README.batch.md README.Cont.md README.Interp.md README.nagios.md NEWS ChangeLog
%config(noreplace) %{_sysconfdir}/%{name}
%{_sbindir}/%{name}
%{perl_vendorlib}/*
# %{_libdir} resolves to /usr/lib64 on 64-bits systems, but the software does not handle this
/usr/lib/%{name}
%{_datadir}/%{name}
%{_datadir}/polkit-1
%{_mandir}/man1/needrestart.1*
%if 0%{?fedora}
%config(noreplace) %{_sysconfdir}/dnf/plugins/needrestart.conf
%{python3_sitelib}
%else
%config(noreplace) %{_sysconfdir}/yum/pluginconf.d/needrestart.conf
/usr/lib/yum-plugins
%endif


%changelog
* Mon Apr 09 2018 Michael Scherer <misc@redhat.com> - 3.1-2
- Add requires on iucode-tool, needed to detect new microcode

* Mon Apr 09 2018 Marc Dequènes (Duck) <duck@redhat.com> - 3.1-1
- NUR: removed all patches, integrated upstream
- added workaround for GetTerminalSize problem when used
  non-interractively (see needrestart#110)

* Tue Jan 09 2018 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-9
- fix crash with YUM plugin when called by yum-cron

* Thu Dec 21 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-8
- forgot the %%{?dist} release component
- fix YUM plugin Python encoding problem
- declare YUM plugin can be run non-interactively
- YUM/DNF plugins: run non-interactively if --assumeyes is used

* Thu Nov 16 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-7
- fix 'check_needed' variable scope in YUM plugin

* Thu Sep 28 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-6
- Thanks Matthias Runge Mauchin for the review
- break description line too long

* Thu Sep 28 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-5
- add dependency on respective YUM/DNF packages to avoid unowned directories

* Wed Sep 27 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-4
- make the changelog more readable

* Fri Sep 22 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-3
- YUM plugin: call needrestart in close_hook to avoid RPMDB mess
- fix conditional to install DNF plugin instead of YUM plugin
- build depends on python3-devel to get the related macros
- fix DNF plugin directory creation

* Fri Sep 15 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-2
- update YUM and DNF plugins: don't crash when needrestart itself is being removed

* Thu Sep 07 2017 Marc Dequènes (Duck) <duck@redhat.com> - 2.11-1
- initial packaging

