# Introduction

This library is in the dependency tree of the `allauth` Python library. The latter is used to authentify via social sites.

Unfortunately a bug prevent the OAuth challenge to complete successfully.

# Reason for Packaging

We need this fixed for social authentication on Mailman 3 installations.

On CentOS 7.3 this bug is still present. This bug is solved in 1.10.4 which is a fixes-only release.

